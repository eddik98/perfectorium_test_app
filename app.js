const express = require('express');
const bodyParser = require('body-parser');
const Router = require('./routes');

const app = express();

app.use(bodyParser.json());

app.use('/queue', Router.queue);
app.use('/message', Router.message);

const server = app.listen(8113, () => {
  const host = server.address().address;
  const { port } = server.address();

  console.log('AWS SQS example app listening at http://%s:%s', host, port);
});
