module.exports = {
  "extends": ["airbnb-base"],
  "plugins": [
    "import"
  ],
  "ecmaFeatures": {
    "destructuring": true,
    "modules": true,
  },
  "env": {
    "es6": true
  },
  "rules": {
    "no-param-reassign": ["error", { "props": false }],
    "nonblock-statement-body-position": ["error", "below"],
  }
};