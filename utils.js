const _ = require('lodash');

module.exports = {
  checkParams: (data, pick) => {
    const result = _.pick(data, pick);

    if (Object.keys(result).length < pick.length) {
      const msg = `Required parameters not passed: ${pick.join(', ')}`;
      throw new Error(msg);
    }

    return result;
  },
};
