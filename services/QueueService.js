const aws = require('aws-sdk');
const config = require('../config');

aws.config.update(config);

const sqs = new aws.SQS();

module.exports = class QueueService {
  constructor() {
    this.defaultUrl = 'https://sqs.us-east-1.amazonaws.com/917787777216/incoming';
  }

  /**
   * Create queue
   *
   * @param {string} name
   * @return {object | Error}
   */
  async create(name) {
    const params = {
      QueueName: name,
    };

    const result = await new Promise((resolve, reject) => {
      sqs.createQueue(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });

    return result;
  }

  /**
   * Send message
   *
   * @param {string} message
   * @param {string} queueUrl
   * @return {object | Error}
   */
  async send(message, queueUrl = '') {
    const params = {
      MessageBody: JSON.stringify(message),
      QueueUrl: queueUrl || this.defaultUrl,
      DelaySeconds: 0,
    };

    const result = await new Promise((resolve, reject) => {
      sqs.sendMessage(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });

    return result;
  }

  /**
   * Receive message
   *
   * @param {string} queueUrl
   * @return {object | Error}
   */
  async receive(queueUrl = '') {
    const params = {
      QueueUrl: queueUrl || this.defaultUrl,
      VisibilityTimeout: 600, // 10 min wait time for anyone else to process.
      AttributeNames: ['All'],
      MaxNumberOfMessages: 10,
    };

    const result = await new Promise((resolve, reject) => {
      sqs.receiveMessage(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });

    return result;
  }

  /**
   * Delete message
   *
   * @param {string} messageId
   * @param {string} queueUrl
   * @param {string} receipt
   * @return {object | Error}
   */
  async delete(messageId, queueUrl = '', receipt = '') {
    const params = {
      Entries: [
        {
          Id: messageId,
          ReceiptHandle: receipt,
        },
      ],
      QueueUrl: queueUrl || this.defaultUrl,
    };

    const result = await new Promise((resolve, reject) => {
      sqs.deleteMessageBatch(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });

    return result;
  }

  /**
   * List queues
   *
   * @return {object | Error}
   */
  async list() {
    const result = await new Promise((resolve, reject) => {
      sqs.listQueues((err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });

    return result;
  }
};
