Start app: pm2 start processes.json
Port: 8011

**Doc api:** 

**Message:**

* Receive message
@method {GET}
@path - /message/receive 
@return {object | Error}
 
* Delete message
@method {DELETE}
@path - /message/delete
@example - {messageId: string}  
@return {object | Error}
  
* Send message
@method {POST}
@path - /message/send
@example - {message: any}   
@return {object | Error}
   
**Queue:**

* Create queue
@method {POST}
@path - /queue/send
@example - {name: string} 
@return {object | Error}
 
* List queues
@method {GET}
@path - /queue/list  
@return {object | Error}