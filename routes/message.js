const express = require('express');
const { checkParams } = require('../utils');
const QS = require('../services/QueueService');

const QueueService = new QS();

const router = express.Router();

/**
 * Send message
 * @method {POST}
 * @path - /message/send
 * @example - {message: any}
 *
 * @return {object | Error}
 */
router.post('/send', async (req, res) => {
  try {
    const { body } = req;

    const data = checkParams(body, ['message']);

    const { message } = data;

    const result = await QueueService.send(message);

    res.send({ error: null, data: result });
  } catch (e) {
    res.send({ error: e.message, data: null });
  }
});

/**
 * Receive message
 * @method {GET}
 * @path - /message/receive
 *
 * @return {object | Error}
 */
router.get('/receive', async (req, res) => {
  try {
    const result = await QueueService.receive();

    res.send({ error: null, data: result });
  } catch (e) {
    res.send({ error: e.message, data: null });
  }
});

/**
 * Delete message
 * @method {DELETE}
 * @path - /message/delete
 * @example - {messageId: string}
 *
 * @return {object | Error}
 */
router.delete('/delete', async (req, res) => {
  try {
    const { body } = req;

    const data = checkParams(body, ['messageId']);

    const { messageId } = data;

    const result = await QueueService.delete(messageId);

    res.send({ error: null, data: result });
  } catch (e) {
    res.send({ error: e.message, data: null });
  }
});

module.exports = router;
