const express = require('express');
const { checkParams } = require('../utils');
const QS = require('../services/QueueService');

const QueueService = new QS();

const router = express.Router();

/**
 * Create queue
 * @method {POST}
 * @path - /queue/send
 * @example - {name: string}
 *
 * @return {object | Error}
 */
router.post('/create', async (req, res) => {
  try {
    const { body } = req;

    const data = checkParams(body, ['name']);

    const { name } = data;

    const result = await QueueService.create(name);

    res.send({ error: null, data: result });
  } catch (e) {
    res.send({ error: e.message, data: null });
  }
});

/**
 * List queues
 * @method {GET}
 * @path - /queue/list
 *
 * @return {object | Error}
 */
router.get('/list', async (req, res) => {
  try {
    const result = await QueueService.list();

    res.send({ error: null, data: result });
  } catch (e) {
    res.send({ error: e.message, data: null });
  }
});

module.exports = router;
