const message = require('./message');
const queue = require('./queue');

module.exports = {
  message,
  queue,
};
